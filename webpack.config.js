var path = require ("path");
var htmlWebPackPlugin = require ("html-webpack-plugin");
var webpack = require ("webpack");
var webpackMerge = require ("webpack-merge");

const modeConfig = (env) => require (`./build-utils/webpack.config.${env}.js`);

module.exports =  ({env, presets} = {env: "production", presets: []}) => {
  return webpackMerge (
    {
      mode: env,
      plugins: [
        // Generates an `index.html` file with the <script> injected.
        new htmlWebPackPlugin ({
          template: "./src/index.html",
          filename: "index.html",
          minify: {
            removeComments: true,
            collapseWhitespace: true,
            removeRedundantAttributes: true,
            useShortDoctype: true,
            removeEmptyAttributes: true,
            removeStyleLinkTypeAttributes: true,
            keepClosingSlash: true,
            minifyJS: true,
            minifyCSS: true,
            minifyURLs: true,
          },
        }),
        new webpack.ProgressPlugin ()
      ]
    },
    modeConfig (env)
  );
}