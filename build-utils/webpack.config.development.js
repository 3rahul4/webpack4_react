const MiniCssExtractPlugin = require ("mini-css-extract-plugin");
const path = require ("path");
var Visualizer = require('webpack-visualizer-plugin');

module.exports = {
    entry: "./src/index.js",
    devtool: "source-map",
    output: {
      path: path.resolve(__dirname, "../dist/"),
      filename: "bundle.js"
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        },
        {
          test: /\.css$/,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: "css-loader",
              options: {
                modules: true,
                importLoaders: 1,
                localIdentName: "[name]_[local]_[hash:base64]",
                sourceMap: true,
                minimize: true
              }
            }]
        },
        {
          test: /\.jpe?g$/,
          use: [{
            loader: "url-loader",
            options: {
              limit: 200000
            }
          }]
        }
      ]
    },
    plugins: [
      new MiniCssExtractPlugin ({
        filename: "bundle.css"
      }),
      new Visualizer()
    ]
}