/* eslint-disable import/no-extraneous-dependencies */
/*
  issue with redux-logger and react-hot-loader
  even tho those 2 deps are only used in development
  eslint has no way to tell that and outputs an error
*/

// react deps
import React from 'react';
import {render} from 'react-dom';
// hot reload for development
import { AppContainer } from 'react-hot-loader';

// redux deps
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

import reducer from './reducers';
import App from './App.jsx';
debugger;
import './style.scss';

const middleware = [thunk];
if (process.env.NODE_ENV !== 'production') middleware.push(logger);

const store = createStore(
  reducer,
  applyMiddleware(...middleware),
);

const root = document.getElementById('root');

const renderC = (Component) => {
  render(
    <AppContainer>
      <Provider store={store}>
        <Component />
      </Provider>
    </AppContainer>,
    root,
  );
};

renderC(App);
//ReactDOM.render(<li>Hi</li>,document.getElementById("root"));

// if (module.hot) {
//   module.hot.accept('./App.jsx', () => { render(App); });
//   module.hot.accept('./reducers', () => { store.replaceReducer(reducer); });
// }
